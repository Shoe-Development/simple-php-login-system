<?php
	use Slim\Slim;
	session_cache_limiter(false);
	session_start();
	
	ini_set('display_errors', 'On');
	
	define('INC_ROOT', dirname(__DIR__));
	
	require INC_ROOT.'/vendor/autoload.php';
	
	$app = new Slim([
			'mode' => file_get_contents(INCROOT.'/mode.php');
		]);
	echo $app->config('mode');

?>